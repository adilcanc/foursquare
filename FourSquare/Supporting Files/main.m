//
//  main.m
//  FourSquare
//
//  Created by adilcan çığ on 27.01.2018.
//  Copyright © 2018 adilcan çığ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
