//
//  VenueModel.m
//  FourSquare
//
//  Created by adilcan çığ on 28.01.2018.
//  Copyright © 2018 adilcan çığ. All rights reserved.
//

#import "VenueModel.h"

@implementation VenueModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"name": @"name",
             @"contact": @"contact",
             @"venueId": @"id",
             @"location": @"location"
             };
}

+ (NSValueTransformer *)locationJSONTransformer
{
    return [NSValueTransformer mtl_JSONDictionaryTransformerWithModelClass:[LocationModel class]];
}

@end
