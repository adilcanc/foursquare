//
//  TipsModel.h
//  FourSquare
//
//  Created by adilcan çığ on 28.01.2018.
//  Copyright © 2018 adilcan çığ. All rights reserved.
//

#import <Mantle/Mantle.h>
#import "LocationModel.h"

@interface TipsModel : MTLModel <MTLJSONSerializing>


@property (nonatomic, copy, readonly) NSArray *items;



@end
