//
//  TipsModel.m
//  FourSquare
//
//  Created by adilcan çığ on 28.01.2018.
//  Copyright © 2018 adilcan çığ. All rights reserved.
//

#import "TipsModel.h"
#import "ItemModel.h"

@implementation TipsModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"items": @"items"
             };
}

+ (NSValueTransformer *)itemsJSONTransformer
{
    return [NSValueTransformer mtl_JSONArrayTransformerWithModelClass:[ItemModel class]];
}

@end
