//
//  VenueModel.h
//  FourSquare
//
//  Created by adilcan çığ on 28.01.2018.
//  Copyright © 2018 adilcan çığ. All rights reserved.
//

#import <Mantle/Mantle.h>
#import "LocationModel.h"

@interface VenueModel : MTLModel <MTLJSONSerializing>


@property (nonatomic, copy, readonly) NSString *name;
@property (nonatomic, copy, readonly) NSString *contact;
@property (nonatomic, copy, readonly) LocationModel *location;
@property (nonatomic, copy, readonly) NSNumber *venueId;


@end
