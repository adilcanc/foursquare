//
//  FSModel.h
//  FourSquare
//
//  Created by adilcan çığ on 27.01.2018.
//  Copyright © 2018 adilcan çığ. All rights reserved.
//

#import <Mantle/Mantle.h>
#import "ResponseModel.h"

@interface FSModel : MTLModel <MTLJSONSerializing>


@property (nonatomic, copy, readonly) ResponseModel *response;


@end
