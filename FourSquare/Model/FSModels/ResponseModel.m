//
//  ResponseModel.m
//  FourSquare
//
//  Created by adilcan çığ on 28.01.2018.
//  Copyright © 2018 adilcan çığ. All rights reserved.
//

#import "ResponseModel.h"

@implementation ResponseModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"venues": @"venues",
             @"tips": @"tips",
             @"geocode": @"geocode"
             };
}

- (instancetype)initWithDictionary:(NSDictionary *)dictionaryValue error:(NSError **)error {
    self = [super initWithDictionary:dictionaryValue error:error];
    if (self == nil) return nil;
    
    // Store a value that needs to be determined locally upon initialization.
    
    return self;
}

+ (NSValueTransformer *)venuesJSONTransformer
{
    return [NSValueTransformer mtl_JSONArrayTransformerWithModelClass:[VenueModel class]];
}

+ (NSValueTransformer *)tipsJSONTransformer
{
    return [NSValueTransformer mtl_JSONDictionaryTransformerWithModelClass:[TipsModel class]];
}

@end
