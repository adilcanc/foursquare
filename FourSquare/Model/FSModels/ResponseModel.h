//
//  ResponseModel.h
//  FourSquare
//
//  Created by adilcan çığ on 28.01.2018.
//  Copyright © 2018 adilcan çığ. All rights reserved.
//

#import <Mantle/Mantle.h>
#import "VenueModel.h"
#import "TipsModel.h"

@interface ResponseModel : MTLModel <MTLJSONSerializing>


@property (nonatomic, copy, readonly) NSArray *venues;
@property (nonatomic, copy, readonly) NSString *geocode;
@property (nonatomic, copy, readonly) TipsModel *tips;


@end
