//
//  FSModel.m
//  FourSquare
//
//  Created by adilcan çığ on 27.01.2018.
//  Copyright © 2018 adilcan çığ. All rights reserved.
//

#import "FSModel.h"

@implementation FSModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"response": @"response"
             };
}

- (instancetype)initWithDictionary:(NSDictionary *)dictionaryValue error:(NSError **)error {
    self = [super initWithDictionary:dictionaryValue error:error];
    if (self == nil) return nil;
    
    // Store a value that needs to be determined locally upon initialization.
    
    return self;
}

+ (NSValueTransformer *)responseJSONTransformer
{
    return [NSValueTransformer mtl_JSONDictionaryTransformerWithModelClass:[ResponseModel class]];
}

@end
