//
//  ItemModel.h
//  FourSquare
//
//  Created by adilcan çığ on 28.01.2018.
//  Copyright © 2018 adilcan çığ. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface ItemModel : MTLModel <MTLJSONSerializing>


@property (nonatomic, copy, readonly) NSString *text;



@end
