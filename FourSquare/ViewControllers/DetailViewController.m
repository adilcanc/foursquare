//
//  DetailViewController.m
//  FourSquare
//
//  Created by adilcan çığ on 28.01.2018.
//  Copyright © 2018 adilcan çığ. All rights reserved.
//

#import "DetailViewController.h"
#import <MapKit/MapKit.h>
#import <Sculptor/Sculptor.h>
#import "ItemModel.h"
#import "FSModel.h"

@interface DetailViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, retain) IBOutlet UIScrollView *scrollView;
@property (nonatomic, retain) UITableView *tableView;
@property (nonatomic, retain) IBOutlet UILabel *nameLabel;
@property (nonatomic, retain) NSArray<ItemModel *> *tipsArray;

@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.nameLabel.text = self.venueModel.name;
    
    // Do any additional setup after loading the view.
    [self setupScrollView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)setupScrollView {
    // set up mapView
    CGRect mapFrame = CGRectMake(0, 0, _scrollView.frame.size.width, 145);
    MKMapView *mapView = [[MKMapView alloc] initWithFrame:mapFrame];
    NSMutableArray *allocations=[[NSMutableArray alloc]init];
    CLLocationCoordinate2D annotationCoord;
    MKPointAnnotation *annotationPoint = [[MKPointAnnotation alloc] init];
    annotationCoord.latitude = [self.venueModel.location.latitude doubleValue ];
    annotationCoord.longitude = [self.venueModel.location.longitude doubleValue];
    annotationPoint.coordinate = annotationCoord;
    annotationPoint.title = self.venueModel.name;
    [allocations addObject:annotationPoint];
    MKCoordinateRegion myRegion;
    MKCoordinateSpan span;
    span.latitudeDelta=0.003;
    span.longitudeDelta=0.003;
    myRegion.center=annotationCoord;
    myRegion.span=span;
    [mapView setRegion:myRegion animated:YES];
    [mapView addAnnotations:allocations];
    [self.scrollView addSubview:mapView];
    
    // set up imageView
    CGRect imageFrame = CGRectMake(0, CGRectGetMaxY(mapView.frame) + 5, _scrollView.frame.size.width, 200);
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:imageFrame];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *getPhotoURL = [NSString stringWithFormat:@"https://api.foursquare.com/v2/venues/%@/photos?client_id=Q5WJQI32MZJPX4ZLHQ2VIGGK1YXPAHSCRWYRAU32ZZZDP51Z&client_secret=GFH3UMXYURA4OFFUELDKTD33PU5HBUKNERCZ4HWXGFZ24GHA&v=20170101",self.venueModel.venueId];
    [manager GET:getPhotoURL parameters:nil success:^(AFHTTPRequestOperation *operation, NSDictionary *user) {
        NSString *photoURL = [NSString stringWithFormat:@"%@360%@",user[@"response"][@"photos"][@"items"][0][@"prefix"],user[@"response"][@"photos"][@"items"][0][@"suffix"]];
        
        NSData * imageData = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString:photoURL]];
        imageView.image = [UIImage imageWithData: imageData];
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        [self.scrollView addSubview:imageView];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error fetching user: %@", error);
    }];
  
    // set up venueTitle
    CGRect labelFrame = CGRectMake(0, CGRectGetMaxY(imageView.frame) + 5, _scrollView.frame.size.width, 40);
    UILabel *venueTitle = [[UILabel alloc] initWithFrame:labelFrame];
    venueTitle.text = self.venueModel.name;
    venueTitle.textAlignment = NSTextAlignmentCenter;
    venueTitle.backgroundColor = [UIColor lightGrayColor];
    [self.scrollView addSubview:venueTitle];
    
    // set up tableView
    CGRect tableFrame = CGRectMake(0, CGRectGetMaxY(venueTitle.frame) + 5, _scrollView.frame.size.width, 500);
    self.tableView = [[UITableView alloc] initWithFrame:tableFrame];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    NSString *getTipsURL = [NSString stringWithFormat:@"https://api.foursquare.com/v2/venues/%@/tips?client_id=Q5WJQI32MZJPX4ZLHQ2VIGGK1YXPAHSCRWYRAU32ZZZDP51Z&client_secret=GFH3UMXYURA4OFFUELDKTD33PU5HBUKNERCZ4HWXGFZ24GHA&v=20170101",self.venueModel.venueId];
    AFHTTPRequestOperationManager *tipManager = [AFHTTPRequestOperationManager manager];
    tipManager.responseSerializer = [SCLMantleResponseSerializer serializerForModelClass:FSModel.class];
    [tipManager GET:getTipsURL parameters:nil success:^(AFHTTPRequestOperation *operation, FSModel *model) {
        self.tipsArray = [[NSArray alloc] initWithArray:model.response.tips.items];
        [self.scrollView addSubview:self.tableView];
        if (self.tipsArray.count > 0) {
            [self.tableView reloadData];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error fetching user: %@", error);
    }];

    self.scrollView.contentSize=CGSizeMake(self.scrollView.frame.size.width, CGRectGetMaxY(self.tableView.frame) + 5);
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.tipsArray.count;
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"cellIdent";
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell==nil) {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        
    }
    cell.textLabel.text=[self.tipsArray objectAtIndex:indexPath.row].text;
    cell.textLabel.numberOfLines = 2;
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 18)];
    /* Create custom view to display section header... */
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, tableView.frame.size.width, 18)];
    [label setFont:[UIFont boldSystemFontOfSize:12]];
    NSString *string = @"Tips";
    /* Section header is in 0th index... */
    [label setText:string];
    [view addSubview:label];
    [view setBackgroundColor:[UIColor colorWithRed:226/255.0 green:226/255.0 blue:226/255.0 alpha:1.0]]; //your background color...
    return view;
}
@end
