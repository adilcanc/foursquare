//
//  TableViewController.m
//  FourSquare
//
//  Created by adilcan çığ on 27.01.2018.
//  Copyright © 2018 adilcan çığ. All rights reserved.
//

#import "PlacesViewController.h"
#import <Sculptor/Sculptor.h>
#import "FSModel.h"
#import "LocationModel.h"
#import "VenueModel.h"
#import "DetailViewController.h"
#import <CCMPopup/CCMPopupSegue.h>
#import <CCMPopup/CCMPopupTransitioning.h>
#import "AppConstants.h"
#import <CoreLocation/CoreLocation.h>

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]


@interface PlacesViewController ()<UITableViewDelegate,CLLocationManagerDelegate>

@property (strong, nonatomic) CLLocationManager *locationManager;
@property (nonatomic, retain) NSArray<VenueModel *> *venueArray;
@property (weak) UIViewController *popupController;
@property (nonatomic,retain) VenueModel *selectedVenueModel;

@end

@implementation PlacesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Places";
    [self setupLocationManager];
    // Do any additional setup after loading the view, typically from a nib.
}
-(void)getVenuesWithRegion:(NSString *)region withLocation:(CLLocation *)location {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer = [SCLMantleResponseSerializer serializerForModelClass:FSModel.class];
    NSString *searchVenuesURL;
    if (self.selectedVenueRegion.length > 0) {
        searchVenuesURL = [NSString stringWithFormat:@"https://api.foursquare.com/v2/venues/search?client_id=%s&client_secret=%s&near=%@&query=%@&v=20170101", FOURSQUARE_CLIENT_ID, FOURSQUARE_CLIENT_SECRET, self.selectedVenueRegion, self.selectedVenueType];
    }
    else {
        searchVenuesURL = [NSString stringWithFormat:@"https://api.foursquare.com/v2/venues/search?client_id=%s&client_secret=%s&ll=%f,%f&query=%@&v=20170101", FOURSQUARE_CLIENT_ID, FOURSQUARE_CLIENT_SECRET, location.coordinate.latitude, location.coordinate.longitude, self.selectedVenueType];
    }
    [manager GET:searchVenuesURL parameters:nil success:^(AFHTTPRequestOperation *operation, FSModel *model) {
        self.venueArray = [[NSArray alloc] initWithArray:model.response.venues];
        [self.tableView reloadData];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error fetching user: %@", error);
    }];
}
-(void)setupLocationManager {
    self.locationManager = [[CLLocationManager alloc]init];
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    self.locationManager.delegate = self;
    if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined ) {
            // We never ask for authorization. Let's request it.
            [self.locationManager requestWhenInUseAuthorization];
            if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse ||
                [CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways) {
                // We have authorization. Let's update location.
                [self.locationManager startUpdatingLocation];
                }
        } else if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse ||
                   [CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways) {
            // We have authorization. Let's update location.
            [self.locationManager startUpdatingLocation];
        } else {
            // If we are here we have no permissions.
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:@"No athorization"
                                         message:@"Please, enable access to your location."
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            
            
            UIAlertAction* yesButton = [UIAlertAction
                                        actionWithTitle:@"Okay"
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action) {
                                            //Handle your yes please button action here
                                        }];
            
            [alert addAction:yesButton];
            
            [self presentViewController:alert animated:YES completion:nil];
        }
    } else {
        // This is iOS 7 case.
        [self.locationManager startUpdatingLocation];
    }
}
- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation {
    [self.locationManager stopUpdatingLocation];
    [self getVenuesWithRegion:self.selectedVenueRegion withLocation:newLocation];
    
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.venueArray.count;
    
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"cellIdent";
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell==nil) {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
        
    }
    cell.textLabel.textColor = UIColorFromRGB(0x462EC5);
    cell.textLabel.text=[self.venueArray objectAtIndex:indexPath.row].name;
    cell.detailTextLabel.text=[self.venueArray objectAtIndex:indexPath.row].location.address;
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 70;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    self.selectedVenueModel = [self.venueArray objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:@"toDetailViewController" sender:self];

}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    CCMPopupSegue *popupSegue = (CCMPopupSegue *)segue;
    if (self.view.bounds.size.height < 420) {
        popupSegue.destinationBounds = CGRectMake(0, 0, ([UIScreen mainScreen].bounds.size.height-20) * .75, [UIScreen mainScreen].bounds.size.height-20);
    } else {
        popupSegue.destinationBounds = CGRectMake(0, 0, 311, 495);
    }
    
    DetailViewController *vc = [segue destinationViewController];
    vc.venueModel = self.selectedVenueModel;
    
    popupSegue.backgroundBlurRadius = 0;
    popupSegue.backgroundViewAlpha = 0.6;
    popupSegue.backgroundViewColor = [UIColor blackColor];
    popupSegue.dismissableByTouchingBackground = YES;
    self.popupController = popupSegue.destinationViewController;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
