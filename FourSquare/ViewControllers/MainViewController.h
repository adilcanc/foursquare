//
//  MainViewController.h
//  FourSquare
//
//  Created by adilcan çığ on 28.01.2018.
//  Copyright © 2018 adilcan çığ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainViewController : UIViewController

-(IBAction)searchVenue:(id)sender;

@end
