//
//  ViewController.h
//  FourSquare
//
//  Created by adilcan çığ on 27.01.2018.
//  Copyright © 2018 adilcan çığ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlacesViewController : UITableViewController

@property (nonatomic, assign) NSString *selectedVenueType;
@property (nonatomic, assign) NSString *selectedVenueRegion;

@end

