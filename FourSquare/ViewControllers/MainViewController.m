//
//  MainViewController.m
//  FourSquare
//
//  Created by adilcan çığ on 28.01.2018.
//  Copyright © 2018 adilcan çığ. All rights reserved.
//

#import "MainViewController.h"
#import "PlacesViewController.h"

@interface MainViewController ()

@property (nonatomic, assign) IBOutlet UITextField *typeTextField;
@property (nonatomic, assign) IBOutlet UITextField *regionTextField;

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Main Page";
    // Do any additional setup after loading the view.
}
-(IBAction)searchVenue:(id)sender {
    if ([self isTypeValid]) {
        [self performSegueWithIdentifier:@"toViewController" sender:self];
    }
    else {
        UIAlertController * alert = [UIAlertController
                                     alertControllerWithTitle:@"Invalid Field"
                                     message:@"Venue Type text must contains only letters and at least 3 characters."
                                     preferredStyle:UIAlertControllerStyleAlert];
        
        
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"Okay"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action) {
                                        //Handle your yes please button action here
                                    }];
        
        [alert addAction:yesButton];

        [self presentViewController:alert animated:YES completion:nil];
    }
}
-(BOOL)isTypeValid {
    //Create character set
    NSCharacterSet *validChars = [NSCharacterSet characterSetWithCharactersInString:@"ABCÇDEFGĞHIİJKLMNOÖPQRSŞTUÜVWXYZabcçdefgğhıijklmnoöpqrstuüvwxyz"];
    
    //Invert the set
    validChars = [validChars invertedSet];
    
    //Check against that
    NSRange  range = [self.typeTextField.text rangeOfCharacterFromSet:validChars];
    if (NSNotFound != range.location) {
        //invalid chars found
        return NO;
    }
    if (self.typeTextField.text.length<3) {
        return NO;
    }
    return YES;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    PlacesViewController *vc = [segue destinationViewController];
    vc.selectedVenueType = self.typeTextField.text;
    vc.selectedVenueRegion = self.regionTextField.text;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
